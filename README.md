# voxl-tunnel

A simple Voxl-to-Voxl network tunnel for ModalLink to allow
an attached subnet of nodes to use the link.

## Voxl side

This is persistent and only needs to be done once
```
# ifconfig eth0 down
# ifconfig eth0 10.0.1.1 netmask 255.255.255.0 up
```

## Subnet side

This is example code for a single Raspberry Pi 3 node on the subnet.
This is not persistent and needs to be done after every reboot.

Set up the ethernet interface
```
$ sudo ifconfig eth0 down
$ sudo ifconfig eth0 10.0.1.2 netmask 255.255.255.0 up
```
Set up the routing table
```
$ sudo route add default gw 10.0.1.1
```

## Configuration file

The configuration file is in JSON format and enumerates all of the nodes in
all of the subnets in the ModalLink network. It also configures the primary
IP address used by the tunnel for communication over the LTE link. Each subnet
must have IP addresses in the 10.0.x.0/24 where all nodes share the same value of
x. The gateway IP address is on the Voxl and must be 10.0.x.1. The subnet nodes
can have any address starting with 10.0.x.2 and continuing until 10.0.x.254.

## Usage

First, create a configuration file in /home/root on the Voxl. Place the voxl-tunnel
executable in /home/root. Configure the network interface on the Voxl and on each
node of the subnet. Configure the routing table on each node of the subnet. Run
the voxl-tunnel executable. Running it in verbose mode shows lots of information
to help in debugging configuration file or communications issues.

## To do list

- Disable forwarding of tunneled packets outside of the tunnel
- Recalculate the mac packet fcs when changing the mac addresses
