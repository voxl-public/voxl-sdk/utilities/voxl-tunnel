#!/usr/bin/python

import sys
import time
import os
import subprocess
import socket
import struct
import threading
import argparse
import json
import binascii

# Program version number
VERSION = '1.0.0'

# Useful constants
IP_ADDRESS_LENGTH = 4
IP_SUBNET_INDEX = 2
IP_NODE_INDEX = 3
MAC_ADDRESS_LENGTH = 6

# Global data
raw_socket = 0
network_socket = 0
running = True

# Data structures for mapping network values to tunnel values and vice versa
my_subnet_id = None
lte_ip_address = {}
subnet_source_mac_address = {}
subnet_destination_mac_address = {}

# Handy function to get our local IP address for the LTE link
def get_ip():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    try:
        # doesn't even have to be reachable
        s.connect(('10.255.255.255', 1))
        IP = s.getsockname()[0]
    except:
        IP = '127.0.0.1'
    finally:
        s.close()
    return IP

# The network thread. This thread handles the reception of tunneled packets
# and forwards them on to the correct subnet endpoint
def network_thread():
    if args.verbose:
        print 'network thread starting'

    # Main network thread processing loop
    while running:

        # Add a timeout to the socket so that we can periodically check
        # to make sure that the program isn't being terminated.
        network_socket.settimeout(1.0)

        try:
            pkt, addr = network_socket.recvfrom(65565)

            # Break out the source and destination ip addresses
            ip_hdr = struct.unpack("!4s4s", pkt[26:34])
            dst_ip = socket.inet_ntoa(ip_hdr[1])

            if args.verbose:
                src_ip = socket.inet_ntoa(ip_hdr[0])
                print 'From LTE: ' + dst_ip + ' ' + src_ip

            # Break the destination ip address into it's sub values
            dst_ip_values = dst_ip.split('.')

            # Verify correct destination
            if dst_ip_values[0] != '10' or \
               dst_ip_values[1] != '0' or \
               dst_ip_values[2] != my_subnet_id:
                if args.verbose:
                    print "subnet doesn't match: " + dst_ip_values[0] + '.' + \
                          dst_ip_values[1]
                continue

            # Make sure we have a source mac address
            destination_subnet = dst_ip_values[IP_SUBNET_INDEX]
            if destination_subnet in subnet_source_mac_address.keys():
                source_mac_address = \
                  subnet_source_mac_address[destination_subnet]
            else:
                if args.verbose:
                    print "subnet index doesn't match for source mac: " + \
                          destination_subnet
                continue

            # Make sure we have a destination mac address
            if destination_subnet in subnet_destination_mac_address.keys():
                if dst_ip in \
                   subnet_destination_mac_address[destination_subnet].keys():
                    destination_mac_address = \
                      subnet_destination_mac_address[destination_subnet][dst_ip]
                else:
                    if args.verbose:
                        print "destination ip not in destination mac table: " + \
                              dst_ip
                    continue
            else:
                if args.verbose:
                    print "subnet index doesn't match for destination mac: " + \
                          destination_subnet
                continue

            # Create the new packet with updated source and destination mac
            # addresses for the subnet
            modified_pkt = destination_mac_address + source_mac_address + \
                           pkt[12:]

            # Send the updated packet out onto the Ethernet
            raw_socket.send(modified_pkt)

        except socket.timeout:
            # if args.verbose:
            #     print 'Got timeout on socket'
            pass

    if args.verbose:
        print 'network thread ending'

##########################
#                        #
# Start of main function #
#                        #
##########################

print 'voxl-tunnel version ' + VERSION

configuration_file_name = 'config.json'

# Parse command line arguments
parser = argparse.ArgumentParser()
parser.add_argument('-v', '--verbose', help='show extra information',
                    action='store_true')
parser.add_argument('-c', '--configuration_file',
                    help=('name of configuration file. default is ' + \
                           configuration_file_name))
args = parser.parse_args()

if args.configuration_file:
    configuration_file_name = args.configuration_file

# Optional verbose details
if args.verbose:
    print 'verbose output chosen'
    print 'using configuration file ' + configuration_file_name

# Open the configuration file
try:
    configuration_file = open(configuration_file_name, 'r')
except IOError as e:
    print 'ERROR: {0}'.format(e.strerror)
    sys.exit(-1)
else:
    if args.verbose:
        print 'opened ' + configuration_file_name

# Decode the JSON in the configuration file
try:
    json_configuration = json.load(configuration_file)
except ValueError:
    print 'ERROR: configuration file json decoding'
    configuration_file.close()
    sys.exit(-1)
else:
    if args.verbose:
        print 'successfully decoded ' + configuration_file_name

# Close the configuration file
configuration_file.close()

# Build the required data structures based on the JSON configuration
subnet_array = json_configuration['configuration']
subnet_array_length = len(subnet_array)
if args.verbose:
    print 'number of subnets: ' + str(subnet_array_length)

my_ip_address = get_ip()
if args.verbose:
    print 'my lte ip address is ' + my_ip_address

# Cycle through each subnet
for i in range(subnet_array_length):
    # The subnet LTE IP address is the IP address of the LTE
    # interface that is connected to the subnet
    subnet_lte_ip_address = json_configuration['configuration'][i]['lte ip']
    if args.verbose:
        print '\tsubnet ' + str(i) + ' LTE IP address ' + subnet_lte_ip_address

    my_subnet_match = False
    if my_ip_address == subnet_lte_ip_address:
        my_subnet_match = True

    subnet_node_array = json_configuration['configuration'][i]['subnet']
    subnet_node_array_length = len(subnet_node_array)
    if args.verbose:
        print '\tsubnet ' + str(i) + ' has ' + \
              str(subnet_node_array_length) + ' nodes'

    # Cycle through each node in the current subnet
    node_subnet_id = '0'
    for j in range(subnet_node_array_length):
        node_ip_addr = subnet_node_array[j]['ip']
        node_mac_addr = subnet_node_array[j]['mac']
        node_subnet_ip_values = node_ip_addr.split('.')

        if args.verbose:
            print '\t\tsubnet ' + str(i) + ' node ' + str(j) + \
                  ' IP address ' + node_ip_addr + \
                  ' MAC address ' + node_mac_addr

        # Error checking
        if len(node_subnet_ip_values) != IP_ADDRESS_LENGTH:
            print 'ERROR, bad ip address length' + node_ip_addr
            sys.exit(-1)
        for k in range(IP_ADDRESS_LENGTH):
            if int(node_subnet_ip_values[k], base=16) > 255:
                print 'ERROR, bad ip address values' + node_ip_addr
                sys.exit(-1)

        # Save the LTE IP address for this subnet into the dictionary
        # that maps subnet id into LTE IP address (with error checking)
        if j == 0:
            node_subnet_id = node_subnet_ip_values[IP_SUBNET_INDEX]
            if node_subnet_id in lte_ip_address.keys():
                print 'ERROR, duplicate subnet'
                sys.exit(-1)
            else:
                lte_ip_address[node_subnet_id] = subnet_lte_ip_address
        else:
            # Make sure all nodes have the same subnet id
            if node_subnet_ip_values[IP_SUBNET_INDEX] != node_subnet_id:
                print 'ERROR, bad node ip subnet value' + node_ip_addr
                sys.exit(-1)

        # Take note of our own subnet id
        if my_subnet_match:
            my_subnet_id = node_subnet_id

        # Convert MAC address string into byte array
        mac_addr_values_array = node_mac_addr.split(':')
        mac_addr_values_array_length = len(mac_addr_values_array)

        # Error checking
        if mac_addr_values_array_length != MAC_ADDRESS_LENGTH:
            print 'ERROR, bad mac address length' + node_mac_addr
            sys.exit(-1)
        for k in range(MAC_ADDRESS_LENGTH):
            if int(mac_addr_values_array[k], base=16) > 255:
                print 'ERROR, bad mac address values' + node_mac_addr
                sys.exit(-1)

        # Convert mac address into binary string that can be added to packets
        tmp_mac_addr = struct.pack('!BBBBBB',
                                   int(mac_addr_values_array[0], base=16),
                                   int(mac_addr_values_array[1], base=16),
                                   int(mac_addr_values_array[2], base=16),
                                   int(mac_addr_values_array[3], base=16),
                                   int(mac_addr_values_array[4], base=16),
                                   int(mac_addr_values_array[5], base=16))

        # Look for the gateway address. If found, save it's MAC address
        # in the dictionary that maps subnet id to source MAC address
        # (with error checking)
        if node_subnet_ip_values[IP_NODE_INDEX] == '1':
            if node_subnet_id in subnet_source_mac_address.keys():
                print 'ERROR, duplicate gateway source mac address'
                sys.exit(-1)
            subnet_source_mac_address[node_subnet_id] = tmp_mac_addr
        # If it isn't the gateway then save the MAC address into the
        # dictionary that maps subnet destination ip addresses into
        # destination mac addresses (with error checking)
        else:
            if node_subnet_id in subnet_destination_mac_address.keys():
                if node_ip_addr in \
                   subnet_destination_mac_address[node_subnet_id].keys():
                    print 'ERROR, duplicate subnet node ip address'
                    sys.exit(-1)
            else:
                subnet_destination_mac_address[node_subnet_id] = {}
            subnet_destination_mac_address[node_subnet_id][node_ip_addr] = \
              tmp_mac_addr

# Summarize the configuration tables
if args.verbose:
    print 'local subnet id ' + my_subnet_id
    print 'lte ip address dictionary: ' + str(lte_ip_address)
    for key in subnet_source_mac_address.keys():
        print 'subnet ' + key + ' source mac address ' + \
              binascii.hexlify(subnet_source_mac_address[key])
    for key in subnet_destination_mac_address.keys():
        print 'subnet ' + key + ' destination mac addresses'
        for subkey in subnet_destination_mac_address[key].keys():
            print '\tdestination ip ' + subkey + ' destination mac address ' + \
                  binascii.hexlify(subnet_destination_mac_address[key][subkey])

# Note: A lot of help in dealing with raw sockets comes from:
# https://gist.github.com/davidlares/e841c0f9d9b31f3cd8859575d061c467

# create a raw socket and bind it to the Ethernet interface
raw_socket = socket.socket(socket.PF_PACKET, socket.SOCK_RAW,
                           socket.htons(0x0800))
raw_socket.bind(('eth0', 0x0800))

# Create a network socket to send / receive packets over the LTE network
network_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
network_socket.bind((get_ip(), 5009))

# Create and start a thread to manage the network socket
net_thread = threading.Thread(target=network_thread)
net_thread.start()

# Main processing loop
try:
    # receive raw packets from the Ethernet interface
    while True:
        pkt, addr = raw_socket.recvfrom(65565)

        # Get the source and destination ip addresses from the packet.
        # We get a full mac packet from the raw socket so have to offset
        # for the extra mac header
        ip_hdr = struct.unpack("!4s4s", pkt[26:34])
        dst_ip = socket.inet_ntoa(ip_hdr[1])

        # Make sure that this packet is destined for a remote subnet
        # that has been configured
        dst_ip_parts = dst_ip.split('.')
        if dst_ip_parts[0] == '10' and \
           dst_ip_parts[1] == '0':
            subnet_id = dst_ip_parts[2]
            # Make sure this isn't a packet for our local subnet
            if subnet_id != my_subnet_id:
                if subnet_id in lte_ip_address.keys():
                    foreign_ip = lte_ip_address[subnet_id]
                    if args.verbose:
                        src_ip = socket.inet_ntoa(ip_hdr[0])
                        print "From Ethernet: " + dst_ip + " " + src_ip
                    network_socket.sendto(pkt, (foreign_ip, 5009))

# Exit cleanly on a ctrl-c or kill
except (KeyboardInterrupt, SystemExit):
    print 'Got ctrl-c, exiting'
    running = False

# Wait for the net thread to exit
net_thread.join()

print 'voxl-tunnel ending'
